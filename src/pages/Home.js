import React from 'react';
import Bulletin from "../components/Bulletin";
import Navbar from '../components/Navbar';
import Slider from '../components/Slider';
import Categories from '../components/Categories';
import Products from "../components/Products";
import Footer from "../components/Footer";

const Home = () => {
	return (
		<div>
		    <Bulletin/>
		    <Navbar/>
           <Slider/>
           <Categories/>
           <Products/>
           <Footer/>

		</div>	


		);
};
export default Home;