export const sliderItems = [
    {
      id: 1,
      img: "https://www.beyoung.in/blog/wp-content/uploads/2020/03/long_skirt_with_crop_top_indian_summer_3-min.jpg",
      title: "SUMMER SALE",
      desc: "DON'T MISS OUT ON WHAT YOU CAN AVAIL! Get 30% Disc. on our NEW Summer Collection.",
      bg: "ffb3b6",
    },
    {
      id: 2,
      img: "https://www.beyoung.in/blog/wp-content/uploads/2020/03/Crop_top_with_Jeans-min.jpg",
      title: "WOMEN'S COLLECTION",
      desc: "CHOOSE YOUR STYLE! GET 20% OFF ON NEW ARRIVALS.",
      bg: "ffcccf",
    },
    {
      id: 3,
      img: "https://pjuractive.com/wp-content/uploads/shutterstock_383170462_Schuhtypen_1.jpg",
      title: "FOOTWEAR COLLECTION",
      desc: "CHOOSE YOUR STYLE! GET FLAT 20% OFF ON NEW ARRIVALS.",
      bg: "ffe6e7",
    },
  ];

  export const categories = [
    {
      id: 1,
      img: "https://cdn.shopify.com/s/files/1/1816/6561/products/h_large.jpg?v=1571440376",
      title: "STYLISH TOPS!",
      cat: "tops"
    },
    {
      id: 2,
      img: "https://i.pinimg.com/236x/87/7f/2b/877f2b93674360b57c6136e5194573d1.jpg",
      title: "BOTTOM WEAR",
      cat: "bottoms"
    },
    {
      id: 3,
      img: "https://www.shoe-tease.com/wp-content/uploads/2019/12/Black-Womens-Waterproof-Combat-Boots-Cougar-Delson.jpg.webp",
      title: "BOOTS",
      cat: "footwear"

    },
  ];

  export const bestSellers = [
    {
      id:1,
      img:"https://cdn.shopify.com/s/files/1/1816/6561/products/TP150_large.jpg?v=1618058345",
    },
    {
      id:2,
      img:"https://cdn.shopify.com/s/files/1/1816/6561/products/27thOctober2020_11_large.jpg?v=1603790186",
    },
    {
      id:3,
      img:"https://cdn.shopify.com/s/files/1/1816/6561/products/Blazing_Blue_Peplum_Top_large.jpg?v=1571440362",
    },
    {
      id:4,
      img:"https://ae01.alicdn.com/kf/HTB1ZLM5a.T1gK0jSZFrq6ANCXXa5/Spring-Summer-Pyjama-Pants-Home-Women-s-Cotton-Sleepwear-Women-Lounge-Wear-Pajama-Pants-Bottom-Wear.jpg_Q90.jpg_.webp",
    },
    {
      id:5,
      img:"https://rukminim1.flixcart.com/image/332/398/ktketu80/shoe/t/e/8/4-ml27-4-sukun-pink-original-imag6vqsdxyr5a9t.jpeg?q=50",
    },
    {
      id:6,
      img:"https://cdn.shopify.com/s/files/1/1816/6561/products/Artboard1copy4_4d808822-ac14-4aa6-9783-0bc6ef2afa11_large.png?v=1641636128",
    },
    {
      id:7,
      img:"https://m.media-amazon.com/images/I/519HHMNx0sL._UL1214_.jpg",
    },
    {
      id:8,
      img:"https://cdn.shopify.com/s/files/1/1816/6561/products/WebsiteImage_15thDec2020_1_large.jpg?v=1608103390",
    },
  ]