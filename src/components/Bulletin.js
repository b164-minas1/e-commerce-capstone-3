import styled from "styled-components";

const Container = styled.div`
  height: 30px;
  background-color: #ff999e;
  color: black;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 16px;
  font-weight: 500;
`;

const Bulletin = () => {
  return <Container>Deal for the Month of MAY! Free Shipping on Orders Over 200</Container>;
};

export default Bulletin;
